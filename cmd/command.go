package main

import (
	"bytes"
	"io"
	"os"
	"os/exec"
)

// Command is a type that contains variable and methods. It is used to execute
// unix command line.
type Command struct {
	isVerbose bool
}

// Verbose is a method that used to set whether the function prints live to
// tdout or not.
func (r *Command) Verbose(b bool) {
	r.isVerbose = b
}

// RunInDir is a method that used to execute command line in particular directory.
// This method returns 2 outputs:
// - output string
// - error
func (r *Command) RunInDir(dir string, name string, arg ...string) (out string, err error) {
	var outBuff bytes.Buffer

	// There are 2 output: stdout (terminal) and buffer (string)
	var writers = make([]io.Writer, 0)
	writers = append(writers, &outBuff)
	if r.isVerbose {
		writers = append(writers, os.Stdout)
	}

	var mw = io.MultiWriter(writers...)

	cmd := exec.Command(name, arg...)
	cmd.Dir = dir
	cmd.Stdout = mw
	cmd.Stderr = mw

	err = cmd.Run()
	out = outBuff.String()
	return
}

// Run runs RunInDir in a current directory. This method returns
// 2 outputs:
// - output string
// - error
func (r *Command) Run(name string, arg ...string) (out string, err error) {
	return r.RunInDir("", name, arg...)
}
